## question-book
A simple app for creating, asking, and answering questions.
Designed so you see everyone else's answers only *after* you answer
which helps to avoid initial bias.
See [Anchoring](http://en.wikipedia.org/wiki/Anchoring).

```
git clone https://JonJagger@bitbucket.org/JonJagger/question-book.git
./question-book/sh/pipe.sh
```
- Your server will be up on port 80.
- To find you public IP address you can use
```
./question-book/sh/my_ip.sh
```

![home](/img/home.png)
![ask](/img/ask.png)
![answer](/img/answer.png)
![read](/img/read.png)

Implemented using [Ruby](https://www.ruby-lang.org/en/).
